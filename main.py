"""
DISCLAIMER: the base of this program was written by: Jan B.
https://github.com/b01jan/Downloads-organizer/blob/master/organize.py

I just took his base code and tried to extend it with some additional features.
"""

# imports:
import os
import shutil
import sys
import json


def create_folders(directories, directory_path):
    """
    This function creates the folders in <directory_path> where the files
    will be moved to.
    :param directories: dictionary, this is a dictionary containing the
    names of the sorted folders and the extensions that correspond to those
    folders.
    :param directory_path: string, this is a string of the path to the
    directory that is to be sorted.
    """
    for key in directories:
        if key not in os.listdir(directory_path):
            os.mkdir(os.path.join(directory_path, key))
    if "OTHER" not in os.listdir(directory_path):
        os.mkdir(os.path.join(directory_path, "OTHER"))


def organize_folders(directories, directory_path):
    """
    This function organizes the files in the specified folder into folders
    :param directories: directories: dictionary, this is a dictionary
    containing the names of the sorted folders and the extensions that
    correspond to those folders.
    :param directory_path: string, this is a string of the path to the
    directory that is to be sorted.
    """
    for file in os.listdir(directory_path):
        if os.path.isfile(os.path.join(directory_path, file)):
            src_path = os.path.join(directory_path, file)
            for key in directories:
                extension = directories[key]
                if file.endswith(extension):
                    dest_path = os.path.join(directory_path, key, file)
                    shutil.move(src_path, dest_path)
                    break


def organize_remaining_files(directory_path):
    """
    This function assigns the file that don't have a corresponding folder to
    the <OTHER> directory.
    :param directory_path: string, this is a string of the path to the
    directory that is to be sorted.
    """
    for file in os.listdir(directory_path):
        if os.path.isfile(os.path.join(directory_path, file)):
            src_path = os.path.join(directory_path, file)
            dest_path = os.path.join(directory_path, "OTHER", file)
            shutil.move(src_path, dest_path)


def organize_remaining_folders(directories, directory_path):
    """
    This function assings the folders within the specified directory to the
    <FOLDER> directory.
    :param directories: directories: dictionary, this is a dictionary
    containing the names of the sorted folders and the extensions that
    corresponds to those folders.
    :param directory_path: string, this is a string of the path to the
    directory that is to be sorted.
    """
    list_dir = os.listdir(directory_path)
    organized_folders = []
    for folder in directories:
        organized_folders.append(folder)
    organized_folders = tuple(organized_folders)
    for folder in list_dir:
        if folder not in organized_folders:
            src_path = os.path.join(directory_path, folder)
            dest_path = os.path.join(directory_path, "FOLDERS", folder)
            try:
                shutil.move(src_path, dest_path)
            except shutil.Error:
                shutil.move(src_path, dest_path + " - copy")
                print("That folder already exists in the destination folder."
                      "\nThe folder is renamed to '{}'".format(folder + " - copy"))


if __name__ == '__main__':
    with open("params.json") as params_json:
        params = json.loads(params_json.read())

    # Debug
    print("Data: ")
    print(params)

    directories = params["directories"]

    print("Directories: ")
    print(directories)

    # if flag for different path is present --> use different path else basepath
    directory_path = params["directory_path"]

    try:
        pass
        # create_folders(directories, directory_path)
        # organize_folders(directories, directory_path)
        # organize_remaining_files(directory_path)
        # organize_remaining_folders(directories, directory_path)
    except shutil.Error:
        print("There was an error trying to move an item to its destination folder")